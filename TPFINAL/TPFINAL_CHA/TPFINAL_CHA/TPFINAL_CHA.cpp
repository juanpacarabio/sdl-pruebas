#include <iostream>
#include <string>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <fstream>
#include <vector>
#include "ventana.h" 
#include "imagenes.h"
#include "update.h"
#include "control.h"

using namespace std;
bool isGameRunning = true;

int main(int argc, char* args[]) {
	initializeSDL();
	initializeFonts();
	imgGame();
	lista_personajes();

	while (isGameRunning) {

		gameFPS();
		updateInput();
		updateGame();
		render();

		SDL_Delay(1000 / FPS);
	}
	return 0;
}