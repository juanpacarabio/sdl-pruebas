#pragma once


void onKeyUp(int keyCode) {
    if (keyCode == SDLK_UP) {
        inputUp = false;
    }
    if (keyCode == SDLK_DOWN) {
        inputDown = false;
    }
    if (keyCode == SDLK_SPACE) {
        inputSpace = false;
    }
    if (keyCode == SDLK_ESCAPE) {
        inputEscape = false;
    }

}

void onKeyDown(int keyCode) {
    if (keyCode == SDLK_UP) {
        inputUp = true;
    }
    if (keyCode == SDLK_DOWN) {
        inputDown = true;
    }
    if (keyCode == SDLK_SPACE) {
        inputSpace = true;
    }
    if (keyCode == SDLK_ESCAPE) {
        inputEscape = true;
    }
}

void updateInput() {
    SDL_Event event;

    while (SDL_PollEvent(&event)) {

        switch (event.type) {
        case SDL_QUIT:
            exit(0);
            break;
        case SDL_KEYUP:
            onKeyUp(event.key.keysym.sym);
            break;
        case SDL_KEYDOWN:
            onKeyDown(event.key.keysym.sym);
            break;

        }
    }
}