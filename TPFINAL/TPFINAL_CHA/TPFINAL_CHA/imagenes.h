#pragma once

using namespace std;
SDL_Color rojo = { 166, 16, 30 };
SDL_Color FightersColor = { 255, 255, 0 };

SDL_Texture* bkgGameTexture;
SDL_Rect bkgGameRect;
SDL_Texture* flechaTexture;
SDL_Surface* mainTitleSurface;
SDL_Surface* control1Surface;
SDL_Surface* control2Surface;
SDL_Texture* mainTitleTexture;
SDL_Texture* control1Texture;
SDL_Texture* control2Texture;
SDL_Rect mainTitleRect;
SDL_Rect control1Rect;
SDL_Rect control2Rect;

//PERSONAJES IMG
SDL_Texture* rainTexture;
SDL_Texture* scorpionTexture;
SDL_Texture* subzeroTexture;
SDL_Texture* sonyaTexture;
SDL_Texture* kanoTexture;
SDL_Texture* goroTexture;
SDL_Texture* raydenTexture;
SDL_Texture* shangtsungTexture;

typedef struct arrow
{
	int x = 100;
	int y = 150;
};
arrow flecha;

void imgGame() {

	string bkgGameImagePath = "assets/bkgGame.jpg";
	string flechaImagePath = "assets/flecha.png";
	string rainImagePath = "assets/rain.png";
	string scorpionImagePath = "assets/scorpion.png";
	string subzeroImagePath = "assets/subzero.png";
	string sonyaImagePath = "assets/sonya.png";
	string kanoImagePath = "assets/kano.png";
	string goroImagePath = "assets/goro.png";
	string raydenImagePath = "assets/rayden.png";
	string shangtsungImagePath = "assets/shangtsung.png";


	bkgGameTexture = IMG_LoadTexture(gameRenderer, bkgGameImagePath.c_str());
	flechaTexture = IMG_LoadTexture(gameRenderer, flechaImagePath.c_str());
	rainTexture = IMG_LoadTexture(gameRenderer, rainImagePath.c_str());
	scorpionTexture = IMG_LoadTexture(gameRenderer, scorpionImagePath.c_str());
	subzeroTexture = IMG_LoadTexture(gameRenderer, subzeroImagePath.c_str());
	sonyaTexture = IMG_LoadTexture(gameRenderer, sonyaImagePath.c_str());
	kanoTexture = IMG_LoadTexture(gameRenderer, kanoImagePath.c_str());
	goroTexture = IMG_LoadTexture(gameRenderer, goroImagePath.c_str());
	raydenTexture = IMG_LoadTexture(gameRenderer, raydenImagePath.c_str());
	shangtsungTexture = IMG_LoadTexture(gameRenderer, shangtsungImagePath.c_str());

	bkgGameRect.x = 0;
	bkgGameRect.y = 0;
	bkgGameRect.w = 1280;
	bkgGameRect.h = 720;

	mainTitleRect.x = 190;
	mainTitleRect.y = 20;
	mainTitleRect.w = 450;
	mainTitleRect.h = 80;

	control1Rect.x = 100;
	control1Rect.y = 630;
	control1Rect.w = 320;
	control1Rect.h = 30;

	control2Rect.x = 100;
	control2Rect.y = 670;
	control2Rect.w = 320;
	control2Rect.h = 30;

	mainTitleSurface = TTF_RenderText_Blended(mainFont, "List Of Fighters", rojo);
	control1Surface = TTF_RenderText_Blended(mainFont, "Press UP/DOWN to select a fighter", rojo);
	control2Surface = TTF_RenderText_Blended(mainFont, "Press (B)/ Spacebar to remove", rojo);

	mainTitleTexture = SDL_CreateTextureFromSurface(gameRenderer, mainTitleSurface);
	control1Texture = SDL_CreateTextureFromSurface(gameRenderer, control1Surface);
	control2Texture = SDL_CreateTextureFromSurface(gameRenderer, control2Surface);



}
void rectCopy(SDL_Texture* texture, int x, int y, SDL_Renderer* renderer) {
	SDL_Rect general;
	general.x = x;
	general.y = y;

	SDL_QueryTexture(texture, NULL, NULL, &general.w, &general.h);
	SDL_RenderCopy(renderer, texture, NULL, &general);
}

int longLista;
vector<string> fighters;

void lista_personajes()
{
	SDL_RenderClear(gameRenderer);
	fstream archivo;
	archivo.open("characters.cfg", ios::in);
	string linea;
	while (getline(archivo, linea))
	{
		fighters.push_back(linea);
		longLista++;
	}
	archivo.close();
}
void listaEnPantalla() {
	//POSICIONES DE LA LISTA
	int ordenLista[] = { 150, 200, 250, 300, 350, 400, 450, 500, 550, 600 };

	//CARGA DE LETRA
	SDL_Surface* FightersSurface;
	SDL_Texture* FightersTexture;

	//FUNCION PARA CARGAR LA LISTA
	for (int i = 0; i < longLista; i++)
	{
		FightersSurface = TTF_RenderText_Blended(mainFont, fighters.at(i).c_str(), FightersColor);
		FightersTexture = SDL_CreateTextureFromSurface(gameRenderer, FightersSurface);
		rectCopy(FightersTexture, 150, ordenLista[i], gameRenderer);
	}
}
int game;

void render() {
	SDL_RenderClear(gameRenderer);

	SDL_RenderCopy(gameRenderer, bkgGameTexture, NULL, &bkgGameRect);
	rectCopy(flechaTexture, flecha.x, flecha.y, gameRenderer);
	SDL_RenderCopy(gameRenderer, mainTitleTexture, NULL, &mainTitleRect);
	SDL_RenderCopy(gameRenderer, control1Texture, NULL, &control1Rect);
	SDL_RenderCopy(gameRenderer, control2Texture, NULL, &control2Rect);
	listaEnPantalla();

	if (game == 0) {
		rectCopy(rainTexture, 600, 200, gameRenderer);
	}
	if (game == 1) {
		rectCopy(scorpionTexture, 400, 50, gameRenderer);
	}
	if (game == 2) {
		rectCopy(subzeroTexture, 400, 50, gameRenderer);
	}
	if (game == 3) {
		rectCopy(sonyaTexture, 600, 50, gameRenderer);
	}
	if (game == 4) {
		rectCopy(kanoTexture, 400, 50, gameRenderer);
	}
	if (game == 5) {
		rectCopy(goroTexture, 400, 50, gameRenderer);
	}
	if (game == 6) {
		rectCopy(raydenTexture, 400, 50, gameRenderer);
	}
	if (game == 7) {
		rectCopy(shangtsungTexture, 600, 50, gameRenderer);
	}

	SDL_RenderPresent(gameRenderer);
}