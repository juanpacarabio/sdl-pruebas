#pragma once

#include <iostream>
#include <string>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <fstream>
#include <vector>
#include "ventana.h" 
#include "imagenes.h"
#include "update.h"
#include "control.h"

using namespace std;
void draw(SDL_Texture* texture, int x, int y, SDL_Renderer* renderer) {
    SDL_Rect dest;
    dest.x = x;
    dest.y = y;

    SDL_QueryTexture(texture, NULL, NULL, &dest.w, &dest.h);
    SDL_RenderCopy(renderer, texture, NULL, &dest);
}

int listLong;
vector<string> fighters;

void lista_personajes()
{

    SDL_RenderClear(gameRenderer);
    fstream archivo;
    archivo.open("characters.cfg", ios::in);
    string line;
    while (getline(archivo, line))
    {
        fighters.push_back(line);
        listLong++;
    }
    archivo.close();
}