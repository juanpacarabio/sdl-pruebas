#pragma once

float deltaTimeInSeconds;
float TimeInSeconds;
float currentTime;
float FPS = 60;

bool inputUp = false;
bool inputDown = false;
bool inputSpace = false;
bool inputEscape = false;

void gameFPS() {
    int previousTime = currentTime;
    currentTime = SDL_GetTicks();
    float deltaTime = currentTime - previousTime;
    deltaTimeInSeconds = deltaTime / 1000;
}

int posicion = 0;
void updateGame() {
    game = posicion;
    if (inputUp && posicion > 0) {
        flecha.y -= 50;
        posicion--;
        cout << posicion;
        SDL_Delay(150);
    }
    if (inputDown && posicion < fighters.size() - 1) {
        flecha.y = flecha.y + 50;
        posicion++;
        cout << posicion;
        SDL_Delay(150);
    }

    //ESTO ELIMINA DE LA LISTA 
    if (inputSpace)
    {
        fighters.at(posicion) = " ";
    }
    if (inputEscape) {
        exit(0);
    }
}