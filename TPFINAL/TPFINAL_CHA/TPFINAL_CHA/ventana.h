#pragma once

SDL_Window* gameWindow;
SDL_Renderer* gameRenderer;
TTF_Font* mainFont;

void initializeFonts() {
    TTF_Init();
    mainFont = TTF_OpenFont("assets/mainFont.TTF", 30);
}

void initializeSDL() {
    SDL_Init(SDL_INIT_EVERYTHING);
    gameWindow = SDL_CreateWindow("MKFINAL", 200, 300, 1280, 720, SDL_WINDOW_SHOWN);
    gameRenderer = SDL_CreateRenderer(gameWindow, -1, 0);
    SDL_RenderClear(gameRenderer);
    SDL_RenderPresent(gameRenderer);
}